<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    protected $message = [
        404 => 'Endpoint not found',
        400 => 'Bad Request',
        500 => 'Unexpected Error'
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof HttpException) {
            $response['error'] = $e->getStatusCode();
        } else {
            $response['error'] = $e->getCode();
        }

        if (env('APP_ENV') == 'local') {
            $response['message'] = $e->getMessage();
        }

        if ($response['error'] == 0) {
            $response['error'] = 500;
        }

        if (empty($response['message']) && !empty($this->message[$response['error']])) {
            $response['message'] = $this->message[$response['error']];
        }


        return response()->json($response, 200);

//                return parent::render($request, $e);
    }
}
