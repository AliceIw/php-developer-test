<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Services\VideoProcessService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class VideoController extends Controller
{

    protected $videoProcess;

    public function __construct(VideoProcessService $videoProcess)
    {
        $this->videoProcess =$videoProcess;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function create(Request $request)
    {

        $uploadedVideo = $request->file('video');

        $this->videoProcess->setUploadedFile($uploadedVideo);

        if (!$this->videoProcess->validate()) {
            throw new \Exception('Wrong File',400);
        }

        return response()->json($this->videoProcess->saveAndAnalyze());
    }
}
