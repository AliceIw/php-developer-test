<?php
/**
 * Created by PhpStorm.
 * User: alice
 * Date: 31/07/2016
 * Time: 11:39
 */

namespace App\Http\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class VideoProcessService
 * @package App\Http\Services
 */
class  VideoProcessService
{
    protected $saveVideoPath ;

    /**
     * @var \getID3|null
     */
    protected $getId3 = null;

    /**
     * @var UploadedFile|null
     */
    protected $uploadedVideo= null;


    /**
     * VideoProcessService constructor.
     * @param \getID3 $getID3
     * @param string $saveVideoPath
     */
    public function __construct(\getID3 $getID3, $saveVideoPath = '/video')
    {
        $this->getId3 = $getID3;
        $this->saveVideoPath = base_path() . $saveVideoPath;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        return (stripos($this->uploadedVideo->getMimeType(), 'video') !== false);
    }

    /**
     * @return array|string
     */
    public function saveAndAnalyze()
    {
        $this->uploadedVideo->move($this->saveVideoPath);

        $videoData = $this->getId3->analyze($this->saveVideoPath .'/'. $this->uploadedVideo->getFilename());

        return $this->array_utf8_encode($videoData);

    }

    /**
     * @param $dat
     * @return array|string
     */
    protected function array_utf8_encode($dat)
    {
        if (is_string($dat))
            return utf8_encode($dat);
        if (!is_array($dat))
            return $dat;
        $ret = array();
        foreach ($dat as $i => $d)
            $ret[$i] = $this->array_utf8_encode($d);
        return $ret;
    }

    /**
     * @param null|UploadedFile $uploadedVideo
     * @return VideoProcessService
     */
    public function setUploadedFile(UploadedFile $uploadedVideo)
    {
        $this->uploadedVideo = $uploadedVideo;
        return $this;
    }
}