<?php
/**
 * Created by PhpStorm.
 * User: alice
 * Date: 31/07/2016
 * Time: 19:50
 */
use  \App\Http\Services\VideoProcessService;
use \Symfony\Component\HttpFoundation\File\UploadedFile;

class VideoProcessServiceTest extends TestCase
{
    /**
     * @var null| VideoProcessService
     */
    protected $videoService = null;
    /**
     * @var null| getID3
     */
    protected $mockGetId3 = null;

    public function setUp()
    {
        parent::setUp();

        $this->mockGetId3 = Mockery::mock('getID3');

        $this->videoService = new VideoProcessService($this->mockGetId3);
    }

    public function test_validate_not_a_video()
    {

        $mockUploadedFile = Mockery::Mock(UploadedFile::class);

        $mockUploadedFile->shouldReceive('getMimeType')->andReturn('image/jpg');

        $this->videoService->setUploadedFile($mockUploadedFile);

        $this->assertEquals($this->videoService->validate(), false);

    }

    public function test_validate_is_a_video()
    {

        $mockUploadedFile = Mockery::Mock(UploadedFile::class);

        $mockUploadedFile->shouldReceive('getMimeType')->andReturn('video/mp4');

        $this->videoService->setUploadedFile($mockUploadedFile);

        $this->assertEquals($this->videoService->validate(), true);

    }


    public function test_saveAndAnalyze()
    {
        $mockUploadedFile = Mockery::Mock(UploadedFile::class);

        $mockUploadedFile->shouldReceive('move')->with(base_path() . '/video');
        $mockUploadedFile->shouldReceive('getFilename')->andReturn('ImAVideo');

        $this->mockGetId3->shouldReceive('analyze')->with(base_path() . '/video/ImAVideo')->andReturn(['Some Data']);

        $this->videoService->setUploadedFile($mockUploadedFile);

        $this->assertEquals($this->videoService->saveAndAnalyze(), ['Some Data']);
    }
}