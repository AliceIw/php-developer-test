<?php
use \Symfony\Component\HttpFoundation\File\UploadedFile;
use \App\Http\Services\VideoProcessService;

class VideoControllerTest extends TestCase
{
    public function setup()
    {
        parent::setUp();


    }

//    public function test_Non_Existing_Url()
//    {
//        $this->get('/v1/nonExistingUrl');
//
//        $this->assertEquals(
//            $this->response->getContent(), '{"error":404,"message":"Endpoint not found"}'
//        );
//    }
//
//    /**
//     * Mask the Error with a generic 500 on non-local environment
//     */
//    public function test_No_file_uploaded()
//    {
//        $this->post('v1/video');
//
//        $this->assertEquals(
//            $this->response->getContent(), '{"error":500,"message":"Unexpected Error"}'
//        );
//    }

    public function test_file_upload()
    {

        $mockUploadedFile = new UploadedFile( base_path().'/video/videoTest.mp4', 'videoTest.mp4', 'video/mp4', 446, null, TRUE );

        $mockVideoService = \Mockery::mock('\App\Http\Services\VideoProcessService');

        $mockVideoService->shouldReceive('setUploadedFile');
        $mockVideoService->shouldReceive('validate')->andReturn(true);
        $mockVideoService->shouldReceive('saveAndAnalyze')->andReturn(['data']);


        $this->app->instance(VideoProcessService::class,$mockVideoService);


        $this->call('POST', 'v1/video', [], [], [
            'video' => $mockUploadedFile
        ]);

        $this->assertEquals(
            $this->response->getContent(), '["data"]'
        );
    }
}
